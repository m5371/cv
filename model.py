from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Users(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.VARCHAR(45))
    password = db.Column(db.VARCHAR(100))

    def __init__(self, name, password):
        self.name = name,
        self.password = password

    def get_id(self):
        return self.id


class Projects(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.VARCHAR)
    description = db.Column(db.VARCHAR)
    repo_link = db.Column(db.VARCHAR)
    tags = db.Column(db.VARCHAR)
    logo = db.Column(db.VARCHAR)
    image = db.Column(db.VARCHAR)

    def __init__(self, title, description, link, tags, logo, image):
        self.title = title,
        self.description = description,
        self.repo_link = link,
        self.tags = tags,
        self.logo = logo,
        self.image = image
