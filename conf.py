from flask import Flask
from model import db, Users
from flask_login import LoginManager


def init_app():
    app = Flask("Infotech Security", static_folder='static', template_folder='templates')
    app.secret_key = "Js6C1q3C1VbJF91rxMfd"
    app.config['JSON_AS_ASCII'] = False

    # Nadanie linku dla bazy danych oraz wyłącznie błędu
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.sqlite'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

    # Inicjalizacja SqlAlchemy (Narzędzia dla Bazy danych)
    db.init_app(app)

    # Inicjalizacja flask-login (Tworzenie instancji LoginManagera dzięki czemu można się logować)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.zaloguj'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(Users_id):
        return Users.query.get(Users_id)

    return app


app = init_app()
