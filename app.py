from flask import render_template, redirect, url_for, flash, request
from flask_login import login_user, login_required, logout_user
from werkzeug.security import check_password_hash
from conf import app
from model import Projects, Users


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/project')
def project():
    output = []
    projects = Projects.query.all()
    for i in projects:
        output.append([i.title, i.description, i.repo_link, i.tags.split(), i.logo, i.image])

    return render_template('project.html', data=output)


@app.route('/add-project', methods=["GET"])
@login_required
def add_project():
    return render_template('add.html')


@app.route('/add-project', methods=["POST"])
@login_required
def add_project_post():
    return redirect(url_for('add_project'))


@app.route('/edit/<id_project>')
@login_required
def edit(id_project):
    return f"Edit {int(id_project)}"


@app.route('/login', methods=['GET'])
def login():
    return render_template('login.html')


@app.route('/login', methods=['POST'])
def login_post():
    name = request.form.get('name')
    password = request.form.get('haslo')

    user = Users.query.filter_by(name=name).first()

    if not user or not check_password_hash(user.password, password):
        flash('Sprawdz swoje dane i spróbuj ponownie', '1')
        return redirect(url_for('login'))

    login_user(user)
    flash("Zalogowano", '2')
    return redirect(url_for('index'))


@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Wylogowano', '2')
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True)
